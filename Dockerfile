FROM alpinelinux/golang:latest as builder

COPY --chown=build: . /home/build/src
WORKDIR /home/build/src

RUN go build

FROM alpine:3.21

RUN adduser -D app

COPY --from=builder --chown=app: /home/build/src/aports-qa-bot /home/build/src/conf.example.json /app/

USER app
WORKDIR /app

EXPOSE 80

CMD ["/app/aports-qa-bot"]
