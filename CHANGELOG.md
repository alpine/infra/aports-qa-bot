# Changelog

All notable changes to this project will be documented in this file.

## [0.4.0] - 2024-11-20

### Bug Fixes

- server: Ignore events that come from the bot itself
- AutoStale: Replace freenode with OFTC

### Features

- server: Add services that use MergeCommentEvent
- AutoMaintainer: Add label when assigning a maintainer
- AutLabeler: Add or remove "NeedsFeedbackLabel" when appropriate
- MaintainerFeedback: Add maintainer feedback service
- AutoStale: Allow to globally configure label and message
- AutoStale: Make contact a global option
- AutoMaintainer: Support maintainer as variable

### Miscellaneous Tasks

- ci: Remove deactivated linters
- Services: Migrate away of deprecated gitlab.String, Time, and Bool
- Services: Migrate away from deprecated GetMergeRequestChanges
- ci: Re-enable deprecations
- ci: Remove linters-settings to set go version
- AutoLabeler: Remove rand.Seed from tests
- ci: Enable all govet checks, and improve some struct alignments
- AutoStale: Some small simplifications and var name improvements
- AutoMaintainer: Fix some tests panicking on error

### Refactor

- Services: Prepare for other types of WebHook services
- server: Slightly simplify logger creation
- Services: Better document and organize structs
- AutoLabeler: Split logic that adds "aports" labels into function
- AutoLabeler: Use slices.Contains instead of custom functions
- Services: Split registration of WebHookServices into functions

## [0.3.4] - 2024-02-17

### Bug Fixes

- conf: Add APIToken, remove GitlabToken
- conf: Remove trailing comma
- conf: Make it clear the config is an example
- MergeRequest: Change Approved for Approval
- AutoLabeler: Use new EventLabel type
- AutoLabeler: Fix new type passed to UpdateMergeRequestOptions
- AutoStale: Fix new type passed to UpdateMergeRequestOptions and ListProjectMergeRequest

### Documentation

- changelog: Generate changelog with git-cliff

### Miscellaneous Tasks

- Add renovate config
- Disable revive var-naming for package names
- Disable unused-parameter
- renovate: Enable semantic commits
- AutoLabeler: Move utils function to service, only used there
- ci: Exclude SA1019 in CI
- docker: Switch to :latest tag

## [0.3.3] - 2023-09-01

### Bug Fixes

- docker: Expose port 80

## [0.3.2] - 2023-08-31

### Docker

- Bump alpine to 3.18

## [0.3.0] - 2023-08-20

### Bug Fixes

- services: Pass WebHookServices by pointer

### Features

- services: Add KDE and alpine-desktop teams to be pinged
- CommentPingTeam: Make the list of teams a configuration option

## [0.2.3] - 2022-12-31

### Bug Fixes

- client: Provide API token

## [0.2.2] - 2022-12-31

### Bug Fixes

- conf: Add missing services

### Features

- services: Log enabled services
- services: Remove WarnProtectedBranch
- server: Restore API token

### Refactor

- server: Rename GitlabToken

## [0.2.1] - 2022-12-26

### Features

- ci: Publish docker image

### Refactor

- ci: Modernize gitlab-ci.yml

## [0.2.0] - 2022-12-25

### Bug Fixes

- server: Make constants typed and rename to conform to GitLab API
- conf: Use int instead of uint
- server: Send HTTP OK as soon as we validate the token
- CancelMergeRequestPipelines: Also cancel pipeline in upstream project
- AutoStale: Remove duplicate field
- server: Fix error message
- CommentOnApproval: Deal in IDs not Usernames
- MergeRequest: Adapt to API changes
- Services: Take a pointer to zerolog.Logger
- GreetFirstContributors: Fix typo in message
- enumer: Fix generation of methods for enums
- server: Use io.ReadAll instead of deprecated ioutil.ReadAll
- *: Use pointers to gitlab.Labels
- server: Rewrite obsolete comment
- server: Return 201 instead of 200, update comment

### Features

- automaintainer: When author and maintainer are the same print `info` instead of `error`
- automaintainer: Strips +tag from email addresses
- main: Use RFC3339 for timestamps
- server: Add Job interface
- AutoLabeler: Rename from autolabeler and implement Job
- AutoMaintainer: Rename from automaintainer and implement Job
- MinimumRequiredSettings: Implement Job
- CancelMergeRequestPipeline: Implement Job
- server: Make use of Job interface and add registeredServices
- MergeRequest: Add new package to represent a MergeRequest for us
- AutoLabeler: Split into a package
- AutoMaintainer: Split into a package
- mocklab: Split into a package
- MinimumRequiredSettings: Split into a package
- CancelMergeRequestPipelines: Split into a package
- conf: Split configuration into a package
- services: Implement a package dedicated to dealing with services
- server: Use facilities to parse WebHooks by go-gitlab
- MergeRequest: Make Action an int and use enumer
- MergeRequest: Make State an int and use enumer
- main: Print the erroneous LogLevel
- Services: Implement knowledge of services including configuration
- AutoStale: Split from client.go into its own service
- Services: Reimplement with support for Poller Services
- GreetFirstContributors: Implement
- Services: Recognize GreetFirstContributors service
- conf: Add GreetFirstContributors to default configuration
- GreetFirstContributors: Add text written by Cogitri
- MergeRequest: Add Approved action
- CommentOnApproval: Implement
- Services: Recognize CommentOnApproval
- conf: Remove AuthenticationToken
- conf: Rename GitlabURL to ProxyURL
- conf: Add ProxyToken
- conf: Remove ProxyToken as it is no longer useful
- WarnProtectedBranch: New service to comment on protected branches
- Services: Recognize WarnProtectedBranch service
- WarnProtectedBranch: Close Merge Request after getting note
- AutoLabeler: Don't give up if getting changes failed
- CommentPingTeam: Add new service

### Miscellaneous Tasks

- gomod: Upgrade go-gitlab to v0.49.0
- gomod: Add github.com/google/uuid
- CHANGES: Add
- gomod: Add enumer
- ci: Fix running of tests
- gomod: Upgrade dependencies
- MergeRequest: Generate
- conf: Adapt to changes
- CHANGES: Write changes up to NEXT
- conf: Update coniguration
- CHANGES: Update with new aports-proxy-bot changes
- conf: Update configuration
- ci: Add golangci-lint
- ci: Add default configuration for running in editors
- ci: Enable security tag, disable specific check
- ci: Add more linters, target go 1.16, autofix issues
- golangci: Fix configuration
- golangci: Disable some tests that are false positives for us
- go: Upgrade go-gitlab and zerolog
- go: Upgrade to 1.18 and tidy it up
- enumer: Update generated code
- tools: Record the tools we use
- ci: Update golangci-lint to v1.45.2
- ci: Show golang and golangci-lint versions
- go: Update dependencies
- go: Reference new repo path under alpine/infra
- AutoMaintainer: Rename variable to reflect purpose
- ci: Upgrade golangci-lint
- CHANGES: Update changes

### Refactor

- automaintainer: Use net/mail to parse email addresses
- client: Convert MergeRequestState into string() before passing
- client: Rename labeler of stale to AutoStale
- client: Make use of new packages
- server: Use new packages
- main: Make use of new packages
- server: Split into its own package
- main: Use new `server` package
- client: Use int instead of uint
- MergeRequest: Use []gitlab.Label instead of our own
- job: Simplify code, remove unnecessary struct
- server: Use new Process() from MergeRequest package
- Services/*: Use new Process from MergeRequest package
- MergeRequest: Remove useless function
- Services: Define the Service interface
- server: Implement ProcessMergeRequestEvent()
- Services/*: Implement the new Service interface
- MergeRequest: Remove obsolete file
- server: Remove test that does not apply anymore
- client: Simplify, make use of go-gitlab where possible
- server: Remove useless casting to string()
- server: Validate State and Int
- client: Use .String() method
- conf: Remove knowledge of services
- client: Use the types from the Services package
- conf: Use the new type from Services
- server: Use the new types from the Services package
- Services: Rename file to match type it provides
- server: Only deal with WebHookServices
- client: Make it ready for multiple poller services
- client: Split into its own client package
- conf: Use the new configuration method
- AutoStale: Use index but assign a variable to it
- server: Downgrade from error to warn on actions we don't know
- server: Move state/action validation outside the for-loop
- CommentOnApproval: Make Info message clearer
- CommentOnApproval: Make Warn message clearer
- CommentOnApproval: Create a template for the note
- client: Use the new configuration options
- server: Use the new configuration options
- *: Perform changes asked by the linter
- *: Fix more linting issues
- main: Stop shadowing variables
- *: Fix mispellings
- AutoLabeler: Don't shadow err
- *: Use pointer to *zerolog.Logger
- AutoMaintainer: Make extractMaintainerEmail public

### Testing

- automaintainer: Test more values for the Maintainer field
- automaintainers: Test new +tag stripping
- AutoLabeler: Conform test filename to service
- AutoMaintainer: Conform test filename to service
- server: Use new packages
- server: Adapt test to new changes
- server: Use the new configuration options
- server: Adapt tests to recent changes

## [0.1.2] - 2021-04-29

### Bug Fixes

- server: Only run AutoMaintainer if there are no assignes

## [0.1.1] - 2021-04-26

### Bug Fixes

- server: Cancel MR pipelines for proper project

### Features

- main: Implement ZeroLog and add LogLevel config
- conf: Add default configuration for LogLevel
- automaintainer: Simplify error messages
- automaintainer: Use zerolog
- client: Use zerolog
- server: Use zerolog

### Miscellaneous Tasks

- go: Add zerolog to dependencies

### Refactor

- autolabel: Simplify errors
- client: Make log messages more structured
- server: Remove redundant break statement
- serve: Remove unused field

### Testing

- autolabel: Adapt tests to new error messages
- server: Fix compilation of tests
- automaintainer: Adapt tests to error message changes

## [0.1.0] - 2021-04-23

### Bug Fixes

- server: Return early if gitlab token isn't valid
- client: Dont mark MRs with status:mr-hold as stale
- client: It's 'developers', not 'alpine-developers'
- client: Restore 30 days as default stale timeout
- client: Fix returning default config if repo has no config file
- server: Only register HTTP handler when listening
- RepoPath: Fix comment on the contains function
- client: Use @team/mentors instead of developers
- server: Only add/remove labels once instead of for every change
- server: Don't try to assign anything if maintainer already set
- automaintainer: Pass a reference to the branch
- automaintainer/server: Use namespaced error messages
- server: Treat Assignee as an array of Users
- server: Only assign on creation or opening

### Documentation

- main|server: Add

### Features

- client: Add polling for stale MRs
- client|server: Add DryRun toggle
- main: Allow overriding default config with conf.override.json
- client: Allow defining config in target repo
- client: Also make staleLabel and notStaleLabels configurable in git repo
- server: Automatically add and remove labels as appropriate
- autolabel: Split some of the automatic labelling into funcs
- RepoPath: Add struct that represents the expected path of changesets
- server: Use RepoPath to decide when labels should be added
- autolabel: Remove most things that are now part of RepoPath
- server: Make HasMove check more robust by checking Repository
- RepoPath: Comment the necessity of the length check
- server: Skip the auto-label loop when not working with APKBUILDs
- server: Document that isUpgrade is ok to be used in this context
- RepoPath: Check the length of all our components for char limit
- server: Add v$branch label for MRs against stable branches
- server: Cancel MR pipelines for merged MRs
- automaintainer: Add helper functions
- server: Implement automaintainer functionality
- mocklab: Add functions to mock Gitlab responses
- main: Implement Services struct with wanted state configuration
- conf: Implement a default Services configuration
- server: Make use of ServiceState configuration
- client: Make use of ServiceState configuration

### Miscellaneous Tasks

- COPYING: Add
- gitignore: Add conf.override.json
- gitlab-ci: Set up tests for autolabel_test.go
- gitlab-ci: Fix file name
- gitlab-ci: Fix image name
- gitlab-ci: Disable cgo to avoid depending on gcc
- server: Update copyright

### Refactor

- RepoPath: Make validRepoNames a unexported variable
- autolabel: Move all of RepoPath into autolabel
- *: Rename APKBUILD member of RepoPath to IsAPKBUILD
- autolabel: Simplify return on ParsePath
- server: Simplify comment
- server: Remove redundant conditionals from auto-labelling
- autolabel: Remove checking for char limits GitLab will handle
- server: Add comment explaining purpose of a huge chunk of code
- server: Explain purpose of a piece of code
- server: Provide rationale for not automatically removing maintainers
- automaintainer: Create function to handle all maintainer logic
- server: Call handleAutoMaintainer() instead of holding all the logic
- mocklab: Remove testMethod function
- automaintainer: Use namespaced and simpler error messages
- automaintainer: Namespace hasUser error messages
- server: Replace Assignee and Author with ID versions
- server: Use `labels` and `assignees` given to us in the webhook
- automaintainer: Use AuthorID to compare author

### Testing

- server: Add tests for testing GitlabToken acceptance
- server: Fix wrong assertion
- autolabel: Add tests for the auto labelling check functions
- autolabel: Remove tests that no longer apply
- RepoPath: Add tests for the ParsePath function
- RepoPath: Tests for components that are too long
- RepoPath: Have the length of the tested string in the test name
- automaintainer: Add tests for hasUser
- autmaintainer: Don't test for methods
- automaintainer: Test extractMaintainerEmail
- automaintainer: Adapt tests to new hasUser error message
- automaintainer: Adapt tests to change in extractMaintainerEmail()
- server: Adapt tests to new configuration type

<!-- generated by git-cliff -->
