// SPDX-FileCopyrightText: 2020-2021 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package conf

import "gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services"

// Options holds all configuration options for the server and poller pat of aports-qa-bot
type Options struct {
	APIToken     string
	WebhookToken string
	ProxyURL     string
	PollerCron   string
	LogLevel     string
	PollerIDs    []int
	Services     Services.KnownServices
	ServerPort   uint
	// We don't want to react to our own events, so we need to know who we are
	BotUserID int
}
