// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package MinimumRequiredSettings

import (
	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

type Service struct {
	gitlabClient *gitlab.Client
	dryRun       bool
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Reopen: true,
	MergeRequest.Open:   true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "MinimumRequiredSettings").
		Logger()

	sLog.Info().Msg("starting")

	if !s.dryRun {
		_, _, err := s.gitlabClient.MergeRequests.UpdateMergeRequest(
			payload.ObjectAttributes.TargetProjectID,
			payload.ObjectAttributes.IID,
			&gitlab.UpdateMergeRequestOptions{
				AllowCollaboration: gitlab.Ptr(true),
				RemoveSourceBranch: gitlab.Ptr(true),
			})
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed")
			return
		}
	}

	sLog.Info().Msg("finished")
}
