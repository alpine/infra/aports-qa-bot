// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package CommentOnApproval

import (
	"fmt"
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

const noteTemplate = "Merge request approved by assigned user @%s"

type Service struct {
	gitlabClient *gitlab.Client
	dryRun       bool
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Approval: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	sLog := log.With().
		Str("service", "CommentOnApproval").
		Logger()

	sLog.Info().Msg("starting")

	// Check if anyone is assigned, exit early and silently if none is
	if payload.ObjectAttributes.AssigneeIDs == nil {
		sLog.Info().Msg("no assignees, finished")
		return
	}

	// Get approvals, this runs only when there is an approval action but to be sure
	// check again
	approvals, _, err := s.gitlabClient.MergeRequestApprovals.GetConfiguration(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to get approvals")
		return
	}

	// List of every approver
	var approver string

	// Iterate over every approver and see if any matches the ID given to us
	for x := range approvals.ApprovedBy {
		if approvals.ApprovedBy[x].User.ID == payload.ObjectAttributes.AssigneeID {
			approver = approvals.ApprovedBy[x].User.Username
		}
	}

	if approver == "" {
		sLog.Warn().Msg("no assignees approved")
		sLog.Info().Msg("finished")
		return
	}

	// Attach the assignee field
	sLog = sLog.With().
		Str("approver", approver).
		Logger()

	// Get our current user
	user, _, err := s.gitlabClient.Users.CurrentUser()
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to get current user")
		return
	}

	// Get all notes of the merge request, to check if we have
	// already commented
	notes, _, err := s.gitlabClient.Notes.ListMergeRequestNotes(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.ListMergeRequestNotesOptions{},
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to get notes")
		return
	}

	noteText := fmt.Sprintf(noteTemplate, approver)

	for y := range notes {
		note := notes[y]

		// Check if we made the note and the text matches
		// we might have made other comments
		if note.Author.Username == user.Username &&
			strings.Contains(
				note.Body,
				noteText,
			) {
			sLog.Info().Msg("already commented")
			return
		}
	}

	sLog.Info().Msg("commenting on approval")

	if !s.dryRun {
		_, _, err = s.gitlabClient.Notes.CreateMergeRequestNote(
			payload.ObjectAttributes.TargetProjectID,
			payload.ObjectAttributes.IID,
			&gitlab.CreateMergeRequestNoteOptions{Body: &noteText},
		)
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed to add approved note to Merge Request")
			return
		}
	}

	sLog.Info().Msg("finished")
}
