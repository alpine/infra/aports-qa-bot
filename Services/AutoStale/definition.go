// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoStale

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

type Service struct {
	gitlabClient *gitlab.Client
	staleLabel   string
	staleMessage string
	staleContact string
	dryRun       bool
}

func New(dryRun bool, gitlabClient *gitlab.Client, staleLabel, staleMessage, staleContact string) Service {
	if staleLabel == "" {
		staleLabel = "status:mr-stale"
	}
	if staleMessage == "" {
		staleMessage = "Sorry to bother you @%s,\n\nbut we've detected that this merge request hasn't seen any recent activity. If you need help or want to discuss your approach with developers you can ping `@%s`. You can also ask on IRC on `#alpine-devel` on OFTC. If no further activity occurs in this MR, Alpine developers may close it in the future.\n\nThanks for your contribution."
	}
	if staleContact == "" {
		staleContact = "team/mentors"
	}
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
		staleLabel:   staleLabel,
		staleMessage: staleMessage,
		staleContact: staleContact,
	}
}

// RepoConfig holds per-repository configuration that's located in a config file in the repo.
type RepoConfig struct {
	StaleLabel     string
	StaleMessage   string
	NotStaleLabels []string
	StaleDays      uint
}

func (s *Service) getConfigForPid(pid int, log *zerolog.Logger) (RepoConfig, error) {
	log.Info().
		Msg("getting project configuration.Configuration")

	config := RepoConfig{
		NotStaleLabels: []string{"status:mr-hold"},
		StaleDays:      30,
		StaleLabel:     s.staleLabel,
		StaleMessage:   s.staleMessage,
	}

	cfgFile, _, err := s.gitlabClient.RepositoryFiles.GetFile(
		pid,
		".gitlab/aports-qa-bot.json",
		&gitlab.GetFileOptions{Ref: gitlab.Ptr("master")},
	)
	if err != nil {
		return config, err
	}

	data, base64Err := base64.StdEncoding.DecodeString(cfgFile.Content)

	if base64Err != nil {
		return config, base64Err
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		return config, err
	}

	log.Info().
		Str("NotStaleLabels", strings.Join(config.NotStaleLabels, ";")).
		Uint("StaleDays", config.StaleDays).
		Str("StaleLabel", config.StaleLabel).
		Str("StaleMessage", config.StaleMessage).
		Msg("got project Configuration")

	return config, nil
}

func (s *Service) pollStale(pid int, repoConfig RepoConfig) ([]*gitlab.MergeRequest, error) {
	labels := gitlab.LabelOptions{repoConfig.StaleLabel}
	labels = append(labels, repoConfig.NotStaleLabels...)
	mrs, _, err := s.gitlabClient.MergeRequests.ListProjectMergeRequests(
		pid,
		&gitlab.ListProjectMergeRequestsOptions{
			State:         gitlab.Ptr(MergeRequest.Opened.String()),
			UpdatedBefore: gitlab.Ptr(time.Now().Add(time.Hour * 24 * time.Duration(repoConfig.StaleDays) * -1)),
			NotLabels:     &labels,
		},
	)
	if err != nil {
		return nil, err
	}
	return mrs, nil
}

// Process processes the MergeRequestStaleJob and may call Gitlab's API to mark a MR as stale
func (s Service) Process(pid int, log *zerolog.Logger) {
	sLog := log.With().
		Str("service", "AutoStale").
		Logger()

	RepoConfig, err := s.getConfigForPid(pid, &sLog)
	if err != nil {
		sLog.Warn().
			Err(err).
			Msg("failed to get repo configuration using default")
	}

	mrs, err := s.pollStale(pid, RepoConfig)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to poll for stale Merge Requests")
		return
	}

	for _, mr := range mrs {
		// Get the right Assignee
		var contact string
		if mr.Assignee != nil {
			contact = mr.Assignee.Username
		} else {
			contact = s.staleContact
		}

		log := sLog.With().Int("MR", mr.IID).Logger()

		log.Info().
			Str("ping", mr.Author.Username).
			Str("contact", contact).
			Msg("processing merge request")

		if !s.dryRun {
			_, _, err := s.gitlabClient.MergeRequests.UpdateMergeRequest(
				mr.TargetProjectID,
				mr.IID, &gitlab.UpdateMergeRequestOptions{
					AddLabels: &gitlab.LabelOptions{RepoConfig.StaleLabel},
				},
			)
			if err != nil {
				log.Error().
					Err(err).
					Msg("failed to add stale label to Merge Request")
				return
			}

			noteText := fmt.Sprintf(
				RepoConfig.StaleMessage,
				mr.Author.Username,
				contact,
			)
			_, _, err = s.gitlabClient.Notes.CreateMergeRequestNote(
				mr.TargetProjectID,
				mr.IID,
				&gitlab.CreateMergeRequestNoteOptions{Body: &noteText},
			)
			if err != nil {
				log.Error().
					Err(err).
					Msg("failed to add stale note to Merge Request")
				return
			}
		}
	}
}
