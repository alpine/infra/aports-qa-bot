// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoLabeler

import "gitlab.com/gitlab-org/api/client-go"

func ContainsLabel(needle string, haystack []*gitlab.EventLabel) bool {
	for _, i := range haystack {
		if i.Title == needle {
			return true
		}
	}
	return false
}
