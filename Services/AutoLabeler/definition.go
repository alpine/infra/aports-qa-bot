// SPDX-FileCopyrightText: 2020-2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoLabeler

import (
	"errors"
	"fmt"
	"regexp"
	"slices"
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

// isUpgrade checks whether the values of the pkgver variable in an APKBUILD have changed
func isUpgrade(
	diff string,
) bool {
	match, _ := regexp.MatchString("\n\\+pkgver\\=", diff)
	if match {
		match, _ := regexp.MatchString("\n-pkgver\\=", diff)
		if match {
			return true
		}
	}
	return false
}

// validRepoNames holds the names of all valid names for repositories
var validRepoNames = []string{"main", "community", "testing", "unmaintained", "non-free"}

// RepoPath holds Repository and PackageDir and APKBUILD. Repository is the name of the
// repository in which the package resides which is acquired by looking at the second to
// last directory component, PackageDir is the directory in which the APKBUILD resides and
// is the last directory component and APKBUILD is a boolean that indicates whether the path
// has APKBUILD as the last component
type RepoPath struct {
	Repository string
	PackageDir string
	IsAPKBUILD bool
}

// ParsePath fills a RepoPath struct with its proper values, it takes one string that must be
// the path and parses it
func ParsePath(path string) (RepoPath, error) {
	result := RepoPath{}

	// We can be given an empty path in cases where a new file is added
	// and we are given OldPath and when a file is removed and NewPath
	// is given
	if path == "" {
		return result, nil
	}

	// Catch any instance that has more than 1 backslash and replace it with only one, then
	// split the string
	re := regexp.MustCompile(`//+`)
	TreatedPath := re.ReplaceAllString(path, "/")
	re = regexp.MustCompile("^/")
	TreatedPath = re.ReplaceAllString(TreatedPath, "")
	re = regexp.MustCompile("/$")
	SplitPath := strings.Split(re.ReplaceAllString(TreatedPath, ""), "/")

	// We need at least 3 components in our path, if there are more then it doesn't
	// really matter we just take the last 3
	if len(SplitPath) < 3 {
		return result, errors.New("not enough elements")
	}

	if SplitPath[len(SplitPath)-1] == "APKBUILD" {
		result.IsAPKBUILD = true
	} else {
		result.IsAPKBUILD = false
	}

	result.PackageDir = SplitPath[len(SplitPath)-2]

	if slices.Contains(validRepoNames, SplitPath[len(SplitPath)-3]) {
		result.Repository = SplitPath[len(SplitPath)-3]
	} else {
		return result, errors.New("repository name isn't valid")
	}
	return result, nil
}

type Service struct {
	gitlabClient            *gitlab.Client
	maintainerFeedbackLabel string
	dryRun                  bool
}

func New(dryRun bool, gitlabClient *gitlab.Client, maintainerFeedbackLabel string) Service {
	return Service{
		dryRun:                  dryRun,
		gitlabClient:            gitlabClient,
		maintainerFeedbackLabel: maintainerFeedbackLabel,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Reopen:   true,
	MergeRequest.Open:     true,
	MergeRequest.Update:   true,
	MergeRequest.Approval: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) aportsLabeler(payload *gitlab.MergeEvent, log *zerolog.Logger) (addLabels, removeLabels gitlab.LabelOptions) {
	diffs, _, err := s.gitlabClient.MergeRequests.ListMergeRequestDiffs(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.ListMergeRequestDiffsOptions{},
	)
	if err != nil {
		log.Error().Msg("failed to get latest changes")
	}

	// Since we don't return on error, this can be a nil, in which case we
	// should absolutely check if the returned value is a nil least we
	// cause a segfault by trying to range over a field of a nil struct
	if diffs != nil {
		var HasMove, HasRemove, HasNew, HasUpgrade bool
		for _, diff := range diffs {
			OldParsedPath, cErr := ParsePath(diff.OldPath)
			if cErr != nil {
				log.Warn().
					Err(cErr).
					Str("old path", diff.OldPath).
					Msg("failed to parse old path of the changeset")
				continue
			}
			NewParsedPath, cErr := ParsePath(diff.NewPath)
			if cErr != nil {
				log.Warn().
					Err(cErr).
					Str("new path", diff.NewPath).
					Msg("failed to parse new path of the changeset")
				continue
			}

			// We only label depending on changes in the APKBUILD
			if !OldParsedPath.IsAPKBUILD && !NewParsedPath.IsAPKBUILD {
				continue
			}

			if diff.RenamedFile &&
				NewParsedPath.IsAPKBUILD &&
				OldParsedPath.Repository != NewParsedPath.Repository {
				HasMove = true
			}
			if diff.DeletedFile {
				HasRemove = true
			}
			if diff.NewFile {
				HasNew = true
			}
			// GitLab API split changes on a per-file basis so it is ok to assume
			// that if the APKBUILD component of OldParsedPath and NewParsedPath
			// are set we are definitively dealing with an APKBUILD
			if OldParsedPath.IsAPKBUILD && NewParsedPath.IsAPKBUILD {
				HasUpgrade = isUpgrade(diff.Diff)
			}

			if !ContainsLabel("aports:move", payload.Labels) && HasMove && !slices.Contains(addLabels, "aports:move") {
				addLabels = append(addLabels, "aports:move")
			} else if ContainsLabel("aports:move", payload.Labels) && !HasMove && !slices.Contains(removeLabels, "aports:move") {
				removeLabels = append(removeLabels, "aports:move")
			}
			if !ContainsLabel("aports:remove", payload.Labels) && HasRemove && !slices.Contains(addLabels, "aports:remove") {
				addLabels = append(addLabels, "aports:remove")
			} else if ContainsLabel("aports:remove", payload.Labels) && !HasRemove && !slices.Contains(removeLabels, "aports:remove") {
				removeLabels = append(removeLabels, "aports:remove")
			}
			if !ContainsLabel("aports:add", payload.Labels) && HasNew && !slices.Contains(addLabels, "aports:add") {
				addLabels = append(addLabels, "aports:add")
			} else if ContainsLabel("aports:add", payload.Labels) && !HasNew && !slices.Contains(removeLabels, "aports:add") {
				removeLabels = append(removeLabels, "aports:add")
			}
			if !ContainsLabel("aports:upgrade", payload.Labels) && HasUpgrade && !slices.Contains(addLabels, "aports:upgrade") {
				addLabels = append(addLabels, "aports:upgrade")
			} else if ContainsLabel("aports:upgrade", payload.Labels) && !HasUpgrade && !slices.Contains(removeLabels, "aports:upgrade") {
				removeLabels = append(removeLabels, "aports:upgrade")
			}
		}
	}

	return addLabels, removeLabels
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	var AddLabels, RemoveLabels gitlab.LabelOptions
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "AutoLabeler").
		Logger()

	sLog.Info().Msg("starting")

	aportsAddLabels, aportsRemoveLabels := s.aportsLabeler(payload, &sLog)
	AddLabels = append(AddLabels, aportsAddLabels[:]...)
	RemoveLabels = append(RemoveLabels, aportsRemoveLabels[:]...)

	// If the one triggering the event is the assignee, then maintainer
	// provided feedback, and we can remove the needs-feedback label
	// We never add the label, since only during asigning a maintainer
	// in AutoMaintainer we are sure that we need maintainer feedback
	if (payload.User.ID == payload.ObjectAttributes.AssigneeID) &&
		ContainsLabel(s.maintainerFeedbackLabel, payload.Labels) {
		RemoveLabels = append(RemoveLabels, s.maintainerFeedbackLabel)
	}

	if strings.Contains(payload.ObjectAttributes.TargetBranch, "-stable") {
		version := strings.Split(payload.ObjectAttributes.TargetBranch, "-")
		AddLabels = append(AddLabels, fmt.Sprintf("v%s", version[0]))
	}

	if len(AddLabels) > 0 {
		sLog.Info().
			Str("labels", strings.Join(AddLabels, ", ")).
			Msg("adding labels")
	}
	if len(RemoveLabels) > 0 {
		sLog.Info().
			Str("labels", strings.Join(RemoveLabels, ", ")).
			Msg("removing labels")
	}

	if !s.dryRun && (len(AddLabels) != 0 || len(RemoveLabels) != 0) {
		_, _, err := s.gitlabClient.MergeRequests.UpdateMergeRequest(
			payload.ObjectAttributes.TargetProjectID,
			payload.ObjectAttributes.IID,
			&gitlab.UpdateMergeRequestOptions{
				AddLabels:    &AddLabels,
				RemoveLabels: &RemoveLabels,
			})
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed to update labels")
			return
		}
	}

	sLog.Info().Msg("finished")
}
