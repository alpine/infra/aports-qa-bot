// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package WarnProtectedBranch

import (
	"fmt"

	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

// We unfortunately cannot use a raw string literal (``) because we have
// to print the triple backticks for a codefence
const note = "Hello there, @%s!" +
	"It appears that you've made this merge request from a protected branch. Unfortunately we can't merge MRs made from such branches." +
	"" +
	"The simplest fix for this is to change the branch from master to some different branch (e.g. $pkgname):" +
	"" +
	"```sh" +
	"git checkout -b $pkgname # Change to the new, unprotected branch" +
	"git push # Simply create a new MR from this branch, Gitlab will print a link to open the MR in your CLI." +
	"```"

type Service struct {
	gitlabClient *gitlab.Client
	dryRun       bool
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Open: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "WarnProtectedBranches").
		Logger()

	sLog.Info().Msg("starting")

	notes, _, err := s.gitlabClient.Notes.ListMergeRequestNotes(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.ListMergeRequestNotesOptions{},
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to get comments on merge request")
		return
	}

	// Get our current user
	us, _, err := s.gitlabClient.Users.CurrentUser()
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to get current user")
		return
	}

	// Create the final note
	noteText := fmt.Sprintf(note, payload.User.Username)

	for x := range notes {
		note := notes[x]
		if note.Author.ID == us.ID && note.Body == noteText {
			sLog.Info().Msg("already commented")
			return
		}
	}

	_, _, err = s.gitlabClient.Notes.CreateMergeRequestNote(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.CreateMergeRequestNoteOptions{Body: &noteText},
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to assign")
		return
	}

	// Close the merge request
	_, _, err = s.gitlabClient.MergeRequests.UpdateMergeRequest(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.UpdateMergeRequestOptions{
			StateEvent: gitlab.Ptr("close"),
		},
		nil,
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to close")
		return
	}
}
