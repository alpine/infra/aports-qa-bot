// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
// SPDX-FileCopyrightText: 2023 Pablo Correa Gómez <ablocorrea@hotmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package Services

import (
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/AutoLabeler"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/AutoMaintainer"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/AutoStale"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/CancelMergeRequestPipelines"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/CommentOnApproval"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/CommentPingTeam"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/GreetFirstContributors"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/MaintainerFeedback"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/MinimumRequiredSettings"
)

// The following "Services" structs are used during the initialization of the
// server from the configuration, to decide which services to create and why.
type KnownServices struct {
	Poller  PollerServices
	WebHook WebHookServices
}

type WebHookServices struct {
	NeedsFeedbackLabel string // We could add a default value here?
	MergeRequest       MergeRequestServices
	MergeComment       MergeCommentServices
}

type MergeRequestServices struct {
	CommentPingTeam             CommentPingTeamService
	MinimumRequiredSettings     State
	CancelMergeRequestPipelines State
	AutoLabeler                 State
	AutoMaintainer              State
	GreetFirstContributors      State
	CommentOnApproval           State
	WarnProtectedBranch         State
}

type CommentPingTeamService struct {
	PingTeams []string
	State     State
}

type MergeCommentServices struct {
	MaintainerFeedback State
}

func RegisterMergeRequestServices(client *gitlab.Client, serviceStates *MergeRequestServices, nfl string) (mr []MergeRequestService) {
	// Instantiate the jobs we know about and add them to an MergeRequestAction map that we run over
	// When adding new services they need to be here
	if serviceStates.AutoMaintainer != Disabled {
		onAssignMaintainerLabels := gitlab.LabelOptions{nfl}
		autoMaintainerJob := AutoMaintainer.New(
			(serviceStates.AutoMaintainer == DryRun),
			client,
			&onAssignMaintainerLabels,
		)
		mr = append(mr, autoMaintainerJob)
		log.Info().
			Str("State", serviceStates.AutoMaintainer.String()).
			Str("onAssignMaintainerLabels", strings.Join(onAssignMaintainerLabels, ", ")).
			Msgf("Registered service AutoMaintainer")
	}
	if serviceStates.CancelMergeRequestPipelines != Disabled {
		cancelMergeRequestPipelinesJob := CancelMergeRequestPipelines.New(
			(serviceStates.CancelMergeRequestPipelines == DryRun), client,
		)
		mr = append(mr, cancelMergeRequestPipelinesJob)
		log.Info().
			Str("State", serviceStates.CancelMergeRequestPipelines.String()).
			Msgf("Registered service CancelMergeRequestPipelines")
	}
	if serviceStates.MinimumRequiredSettings != Disabled {
		minimumRequiredSettingsJob := MinimumRequiredSettings.New(
			(serviceStates.MinimumRequiredSettings == DryRun), client,
		)
		mr = append(mr, minimumRequiredSettingsJob)
		log.Info().
			Str("State", serviceStates.MinimumRequiredSettings.String()).
			Msgf("Registered service MinimumRequiredSettings")
	}
	if serviceStates.AutoLabeler != Disabled {
		autoLabelerJob := AutoLabeler.New(
			(serviceStates.AutoLabeler == DryRun),
			client,
			nfl,
		)
		mr = append(mr, autoLabelerJob)
		log.Info().
			Str("State", serviceStates.AutoLabeler.String()).
			Str("maintainerFeedbackLabel", nfl).
			Msgf("Registered service AutoLabeler")
	}
	if serviceStates.GreetFirstContributors != Disabled {
		greetFirstContributorsJob := GreetFirstContributors.New(
			(serviceStates.GreetFirstContributors == DryRun), client,
		)
		mr = append(mr, greetFirstContributorsJob)
		log.Info().
			Str("State", serviceStates.GreetFirstContributors.String()).
			Msgf("Registered service GreetFirstContributors")
	}
	if serviceStates.CommentOnApproval != Disabled {
		commentOnApprovalJob := CommentOnApproval.New(
			(serviceStates.CommentOnApproval == DryRun), client,
		)
		mr = append(mr, commentOnApprovalJob)
		log.Info().
			Str("State", serviceStates.CommentOnApproval.String()).
			Msgf("Registered service CommentOnApproval")
	}
	if serviceStates.CommentPingTeam.State != Disabled {
		commentPingTeamJob := CommentPingTeam.New(
			(serviceStates.CommentPingTeam.State == DryRun),
			client,
			serviceStates.CommentPingTeam.PingTeams,
		)
		mr = append(mr, commentPingTeamJob)
		log.Info().
			Str("State", serviceStates.CommentPingTeam.State.String()).
			Msgf("Registered service CommentPingTeam")
	}
	return mr
}

func RegisterMergeCommentServices(client *gitlab.Client, serviceStates *MergeCommentServices, nfl string) (mrc []MergeCommentService) {
	if serviceStates.MaintainerFeedback != Disabled {
		maintainerFeedbackJob := MaintainerFeedback.New(
			(serviceStates.MaintainerFeedback == DryRun),
			client,
			nfl,
		)
		mrc = append(mrc, maintainerFeedbackJob)
		log.Info().
			Str("State", serviceStates.MaintainerFeedback.String()).
			Str("maintainerFeedbackLabel", nfl).
			Msgf("Registered service MaintainerFeedback")
	}
	return mrc
}

type PollerServices struct {
	// This one is from the Poller
	AutoStale AutoStaleService
}

type AutoStaleService struct {
	StaleLabel   string
	StaleMessage string
	StaleContact string
	State        State
}

func RegisterPollerServices(client *gitlab.Client, serviceStates PollerServices) (r []PollerService) {
	if serviceStates.AutoStale.State != Disabled {
		autoStaleJob := AutoStale.New(
			(serviceStates.AutoStale.State == DryRun),
			client,
			serviceStates.AutoStale.StaleLabel,
			serviceStates.AutoStale.StaleMessage,
			serviceStates.AutoStale.StaleContact,
		)
		r = append(r, autoStaleJob)
	}
	return r
}

// Every WebHookService requires a Process function that takes:
//  1. the corresponding event pointer as defined by the generic. This event
//     should contain all the information necessary for the service to do its
//     job
//  2. a `zerolog.Logger` struct that has fields associated with it including
//     an uuid that is used to associate all messages generated by the processor
//     and any services that it starts
type WebHookService[T gitlab.MergeEvent | gitlab.MergeCommentEvent] interface {
	Process(event *T, log *zerolog.Logger)
}

// MergeRequestService defines an interface that must be fulfilled to be considered
// a service to be run against a Merge Request event.
//
// The GetStates and GetActions functions are used to determine in which
// states (Open, Closed, Merged) and actions (Open, Reopen, Close, Merge)
// a service will run
type MergeRequestService interface {
	WebHookService[gitlab.MergeEvent]
	GetStates() map[MergeRequest.State]bool
	GetActions() map[MergeRequest.Action]bool
}

// MergeCommentService defines an interface that must be fulfilled to be
// considered a service to be run against a Merge Request Comment event.
type MergeCommentService interface {
	WebHookService[gitlab.MergeCommentEvent]
}

// PollerService interface defines the interface that services that run on
// a polling basis should have. The process function takes the "pollerid"
// and a logging struct associated with the polling object.
type PollerService interface {
	Process(pid int, log *zerolog.Logger)
}
