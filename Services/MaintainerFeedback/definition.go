// SPDX-FileCopyrightText: 2023 Pablo Correa Gómez <ablocorrea@hotmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package MaintainerFeedback

import (
	"strings"

	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/AutoLabeler"
)

type Service struct {
	gitlabClient            *gitlab.Client
	maintainerFeedbackLabel string
	dryRun                  bool
}

func New(dryRun bool, gitlabClient *gitlab.Client, maintainerFeedbackLabel string) Service {
	return Service{
		dryRun:                  dryRun,
		gitlabClient:            gitlabClient,
		maintainerFeedbackLabel: maintainerFeedbackLabel,
	}
}

func (s Service) Process(payload *gitlab.MergeCommentEvent, log *zerolog.Logger) {
	var RemoveLabels gitlab.LabelOptions

	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "MaintainerFeedback").
		Logger()

	sLog.Info().Msg("starting")

	// TODO: which is the value if there's nobody assigned?
	if payload.MergeRequest.AssigneeID != 0 {
		log.Info().Msg("No assignee, ignoring maintainer labels")
		return
	}

	// If the one triggering the event is the assignee, then maintainer
	// provided feedback!
	if (payload.User.ID == payload.MergeRequest.AssigneeID) &&
		AutoLabeler.ContainsLabel(s.maintainerFeedbackLabel, payload.MergeRequest.Labels) {
		RemoveLabels = append(RemoveLabels, s.maintainerFeedbackLabel)
	}

	if len(RemoveLabels) > 0 {
		sLog.Info().
			Str("labels", strings.Join(RemoveLabels, ", ")).
			Msg("removing labels")
	}

	if !s.dryRun && len(RemoveLabels) != 0 {
		_, _, err := s.gitlabClient.MergeRequests.UpdateMergeRequest(
			payload.MergeRequest.TargetProjectID,
			payload.MergeRequest.IID,
			&gitlab.UpdateMergeRequestOptions{
				RemoveLabels: &RemoveLabels,
			})
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed to update labels")
			return
		}
	}

	sLog.Info().Msg("finished")
}
