// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package GreetFirstContributors

import (
	"fmt"

	"github.com/rs/zerolog"
	"gitlab.com/gitlab-org/api/client-go"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

const note = `Hello there, @%s!

Since this is your first contribution to aports, let me introduce you to @team/mentors!

They're a group of Alpine developers that helps new contributors to aports. They have already been notified to check out this MR and help you get your first MR merged quickly and should reach out to you soon in this MR.

If you need their help in the future, don't refrain from pinging them like I did! :)`

type Service struct {
	gitlabClient *gitlab.Client
	dryRun       bool
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Open: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "GreetFirstContributors").
		Logger()

	sLog.Info().Msg("starting")

	mr, _, err := s.gitlabClient.MergeRequests.GetMergeRequest(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.GetMergeRequestsOptions{},
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed")
		return
	}

	if mr.FirstContribution && !s.dryRun {
		noteText := fmt.Sprintf(note, mr.Author.Username)
		_, _, err := s.gitlabClient.Notes.CreateMergeRequestNote(
			mr.ProjectID,
			mr.IID,
			&gitlab.CreateMergeRequestNoteOptions{Body: &noteText},
		)
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed")
			return
		}
	}

	sLog.Info().Msg("finished")
}
