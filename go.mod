module gitlab.alpinelinux.org/alpine/infra/aports-qa-bot

go 1.22.0

toolchain go1.23.4

require (
	github.com/dmarkham/enumer v1.5.11
	github.com/google/uuid v1.6.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/rs/zerolog v1.33.0
	gitlab.com/gitlab-org/api/client-go v0.123.0
)

require (
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pascaldekloe/name v1.0.1 // indirect
	golang.org/x/mod v0.23.0 // indirect
	golang.org/x/oauth2 v0.25.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/time v0.9.0 // indirect
	golang.org/x/tools v0.30.0 // indirect
)
